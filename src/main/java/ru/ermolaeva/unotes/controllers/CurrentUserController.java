package ru.ermolaeva.unotes.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.ermolaeva.unotes.entities.User;
import ru.ermolaeva.unotes.forms.UserForm;
import ru.ermolaeva.unotes.services.UsersService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/currentUser")
public class CurrentUserController {

    private final UsersService usersService;

    @GetMapping
    public String getCurrentUserEditingPage(Model model) {
        Long id = ((User)(SecurityContextHolder.getContext().getAuthentication()).getPrincipal()).getId();

        model.addAttribute("user", usersService.getUserById(id));
        return "current-user";
    }

    @PostMapping("/update")
    public String updateCurrentUser(@RequestParam("currentUserId") Long id, UserForm form) {
        usersService.updateUser(id, form);
        return "redirect:/homePage";
    }
}

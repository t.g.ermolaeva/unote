package ru.ermolaeva.unotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/error")
public class ErrorsController {

    @GetMapping("/404")
    public String get404Page() {
        return "errors/error-404";
    }

    @GetMapping("/loginError")
    public String getLoginErrorPage() {
        return "errors/login-error";
    }
}

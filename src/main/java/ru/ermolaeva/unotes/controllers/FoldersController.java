package ru.ermolaeva.unotes.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ermolaeva.unotes.entities.Folder;
import ru.ermolaeva.unotes.entities.User;
import ru.ermolaeva.unotes.forms.FolderForm;
import ru.ermolaeva.unotes.services.FoldersService;
import ru.ermolaeva.unotes.services.UsersService;

@Controller
@RequiredArgsConstructor
@RequestMapping("/folders")
public class FoldersController {

    @Autowired
    private final FoldersService foldersService;

    @Autowired
    private final UsersService usersService;

    @PostMapping("/new")
    public String create(FolderForm form) {
        foldersService.createFolder(form, usersService.getCurrentUser());
        return "redirect:/homePage";
    }

    @GetMapping("/{folderId}")
    public String getFolderPage(Model model, @PathVariable("folderId") Long id) {
        model.addAttribute("folder", foldersService.getFolderById(id));
        return "folders/edit-folder";
    }

    @PostMapping("/{folderId}/edit")
    public String edit(FolderForm form, @PathVariable("folderId") Long id) {
        foldersService.editFolder(form, id);
        return "redirect:/notes/folder/" + id;
    }

    @PostMapping("/{folderId}/delete")
    public String delete(@PathVariable("folderId") Long id) {
        foldersService.deleteFolder(id);
        return "redirect:/homePage";
    }
}

package ru.ermolaeva.unotes.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.ermolaeva.unotes.services.FoldersService;
import ru.ermolaeva.unotes.services.UsersService;

@Controller
@RequiredArgsConstructor
@RequestMapping("/homePage")
public class HomePageController {

    private final UsersService usersService;
    private final FoldersService foldersService;

    @GetMapping()
    public String getHomePage(Model model) {
        model.addAttribute("user", usersService.getCurrentUser());
        model.addAttribute("folders", foldersService.getAllFoldersByUser(usersService.getCurrentUser()));
        return "home-page";
    }
}

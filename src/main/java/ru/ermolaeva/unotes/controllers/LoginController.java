package ru.ermolaeva.unotes.controllers;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import ru.ermolaeva.unotes.entities.User;
import ru.ermolaeva.unotes.forms.UserForm;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String getLoginPage(Authentication authentication) {
        if (authentication != null) {
            return "redirect:/";
        }

        return "login";
    }

    @GetMapping("/authPage")
    public String getAuthPage() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))) {
                return "redirect:/users";
            } else {
                return "redirect:/homePage";
            }
        }
        return "login";
    }
}

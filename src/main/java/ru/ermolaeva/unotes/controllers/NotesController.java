package ru.ermolaeva.unotes.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ermolaeva.unotes.forms.NoteForm;
import ru.ermolaeva.unotes.services.FoldersService;
import ru.ermolaeva.unotes.services.NotesService;
import ru.ermolaeva.unotes.services.UsersService;

@Controller
@RequiredArgsConstructor
@RequestMapping("/notes")
public class NotesController {

    private final NotesService notesService;
    private final UsersService usersService;
    private final FoldersService foldersService;

    @GetMapping()
    public String getAllNotes(Model model) {
        model.addAttribute("notes", notesService.getAllNotesByUser(usersService.getCurrentUser()));
        return "notes/notes";
    }

    @GetMapping("/folder/{folderId}")
    public String getAllNotesByFolder(Model model,
                                      @PathVariable("folderId") Long folderId) {
        model.addAttribute("folder", foldersService.getFolderById(folderId));
        model.addAttribute("notes", notesService.getAllNotesByFolderId(folderId));
        return "notes/notes-by-folder";
    }

    @GetMapping("/new")
    public String newNotePage(Model model,
                              @RequestParam(value = "folderId", required = false, defaultValue = "1") Long folderId) {
        model.addAttribute("folders", foldersService.getAllFoldersByUser(usersService.getCurrentUser()));
        model.addAttribute("folder", notesService.getAllNotesByFolderId(folderId));
        return "notes/new-note";
    }

    @PostMapping("/create")
    public String create(NoteForm form,
                         @RequestParam(value = "folderId", required = false, defaultValue = "1") Long folderId) {
        notesService.create(form, folderId, usersService.getCurrentUser());
        return "redirect:/notes/folder/" + folderId;
    }

    @GetMapping("/show/{noteId}")
    public String showNotePageByFolder(Model model,
                                       @PathVariable("noteId") Long id) {
        model.addAttribute("folders", foldersService.getAllFoldersByUser(usersService.getCurrentUser()));
        model.addAttribute("folder", notesService.getNoteById(id).getFolder());
        model.addAttribute("note", notesService.getNoteById(id));
        return "notes/read-note";
    }

    @GetMapping("/edit/{noteId}")
    public String getNotePageForEdit(Model model,
                                     @PathVariable("noteId") Long id) {
        model.addAttribute("folders", foldersService.getAllFoldersByUser(usersService.getCurrentUser()));
        model.addAttribute("folder", notesService.getNoteById(id).getFolder());
        model.addAttribute("note", notesService.getNoteById(id));
        return "notes/edit-note";
    }

    @PostMapping("/edit/{noteId}")
    public String edit(NoteForm form,
                       @PathVariable("noteId") Long id,
                       @RequestParam("folderId") Long folderId) {
        notesService.edit(form, id, folderId);

        return "redirect:/notes/folder/" + folderId;
    }

    @PostMapping("/delete/{noteId}")
    public String delete(@RequestParam(value = "folderId", required = false, defaultValue = "0") Long folderId,
                         @PathVariable("noteId") Long id) {
        notesService.delete(id);

        if (folderId != 0)
            return "redirect:/notes/folder/" + folderId;
        else
            return "redirect:/notes";
    }

    @GetMapping("/search")
    public String searchByWord(Model model,
                                 @RequestParam(value = "folderId", required = false, defaultValue = "0") Long folderId,
                                 @RequestParam("word") String word) {
        model.addAttribute("notes", notesService.getNotesBasedOnWord(word));

        if (folderId != 0) {
            model.addAttribute("folder", foldersService.getFolderById(folderId));
            return "notes/notes-by-folder";
        } else {
            return "notes/notes";
        }
    }
}

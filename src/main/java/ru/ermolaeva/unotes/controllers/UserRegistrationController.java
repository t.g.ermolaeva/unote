package ru.ermolaeva.unotes.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.ermolaeva.unotes.forms.UserRegistrationForm;
import ru.ermolaeva.unotes.services.UserRegistrationService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/registration")
public class UserRegistrationController {

    private final UserRegistrationService userRegistrationService;

    @GetMapping
    public String getRegistrationPage(Authentication authentication) {
        if (authentication != null) {
            return "redirect:/";
        }

        return "registration";
    }

    @PostMapping
    public String registerNewUser(UserRegistrationForm form) {
        userRegistrationService.registerUser(form);
        return "redirect:/login";
    }
}

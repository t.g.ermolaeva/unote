package ru.ermolaeva.unotes.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ermolaeva.unotes.services.UsersService;

@Controller
@RequiredArgsConstructor
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private final UsersService usersService;

    @GetMapping
    public String getUsersPage(Model model) {
        model.addAttribute("users", usersService.getAllUsers());
        return "users";
    }

    @PostMapping("/delete")
    public String deleteUser(@RequestParam("userId") Long id) {
        usersService.deleteUser(id);
        return "redirect:/users";
    }
}

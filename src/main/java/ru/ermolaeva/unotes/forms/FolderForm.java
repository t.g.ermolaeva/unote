package ru.ermolaeva.unotes.forms;

import lombok.Data;

@Data
public class FolderForm {
    private String name;
}

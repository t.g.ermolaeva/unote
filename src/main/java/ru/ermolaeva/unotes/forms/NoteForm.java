package ru.ermolaeva.unotes.forms;

import lombok.Data;

@Data
public class NoteForm {
    private String name;
    private String note;
    private Long folderId;
}

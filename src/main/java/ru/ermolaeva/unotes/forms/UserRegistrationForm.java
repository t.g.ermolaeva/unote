package ru.ermolaeva.unotes.forms;

import lombok.Data;

@Data
public class UserRegistrationForm {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
}

package ru.ermolaeva.unotes.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ermolaeva.unotes.entities.Folder;
import ru.ermolaeva.unotes.entities.User;

import java.util.List;

public interface FoldersRepository extends JpaRepository<Folder, Long> {
    List<Folder> findAllByUserOrderByName(User user);
}

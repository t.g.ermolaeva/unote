package ru.ermolaeva.unotes.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ermolaeva.unotes.entities.Note;
import ru.ermolaeva.unotes.entities.User;

import java.util.List;

public interface NotesRepository extends JpaRepository<Note, Long> {

    List<Note> findAllByUserOrderByCreateDate(User user);

    List<Note> findAllByFolderIdOrderByCreateDate(Long folderId);
}

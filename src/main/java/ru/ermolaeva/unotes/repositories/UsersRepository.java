package ru.ermolaeva.unotes.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ermolaeva.unotes.entities.User;

import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String email);
}

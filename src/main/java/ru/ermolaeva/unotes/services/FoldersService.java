package ru.ermolaeva.unotes.services;

import ru.ermolaeva.unotes.entities.Folder;
import ru.ermolaeva.unotes.entities.User;
import ru.ermolaeva.unotes.forms.FolderForm;

import java.util.List;

public interface FoldersService {
    void createFolder(FolderForm form, User user);

    List<Folder> getAllFoldersByUser(User user);

    Folder getFolderById(Long id);

    void editFolder(FolderForm form, Long id);

    void deleteFolder(Long id);
}

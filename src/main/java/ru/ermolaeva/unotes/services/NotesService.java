package ru.ermolaeva.unotes.services;

import ru.ermolaeva.unotes.entities.Note;
import ru.ermolaeva.unotes.entities.User;
import ru.ermolaeva.unotes.forms.NoteForm;

import java.util.List;

public interface NotesService {
    void create(NoteForm form, Long folderId, User user);

    List<Note> getAllNotesByUser(User user);

    Note getNoteById(Long id);

    void edit(NoteForm form, Long id, Long folderId);

    List<Note> getAllNotesByFolderId(Long folderId);

    void delete(Long id);

    List<Note> getNotesBasedOnWord(String word);
}

package ru.ermolaeva.unotes.services;

import ru.ermolaeva.unotes.forms.UserRegistrationForm;

public interface UserRegistrationService {
    void registerUser(UserRegistrationForm form);
}

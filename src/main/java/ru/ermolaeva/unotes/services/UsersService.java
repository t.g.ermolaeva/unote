package ru.ermolaeva.unotes.services;

import ru.ermolaeva.unotes.entities.User;
import ru.ermolaeva.unotes.forms.UserForm;

import java.util.List;

public interface UsersService {

    User getCurrentUser();

    List<User> getAllUsers();

    void deleteUser(Long id);

    User getUserById(Long id);

    void updateUser(Long id, UserForm form);
}

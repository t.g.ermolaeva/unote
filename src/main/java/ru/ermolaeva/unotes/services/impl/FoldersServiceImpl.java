package ru.ermolaeva.unotes.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ermolaeva.unotes.entities.Folder;
import ru.ermolaeva.unotes.entities.User;
import ru.ermolaeva.unotes.forms.FolderForm;
import ru.ermolaeva.unotes.repositories.FoldersRepository;
import ru.ermolaeva.unotes.services.FoldersService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FoldersServiceImpl implements FoldersService {

    @Autowired
    private final FoldersRepository foldersRepository;

    @Override
    public void createFolder(FolderForm form, User user) {
        Folder folder = Folder.builder()
                .name(form.getName())
                .user(user)
                .build();

        foldersRepository.save(folder);
    }

    @Override
    public List<Folder> getAllFoldersByUser(User user) {
        return foldersRepository.findAllByUserOrderByName(user);
    }

    @Override
    public Folder getFolderById(Long id) {
        return foldersRepository.getById(id);
    }

    @Override
    public void editFolder(FolderForm form, Long id) {
        Folder folder = foldersRepository.getById(id);

        folder.setName(form.getName());
        foldersRepository.save(folder);
    }

    @Override
    public void deleteFolder(Long id) {
        foldersRepository.deleteById(id);
    }
}

package ru.ermolaeva.unotes.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.ermolaeva.unotes.entities.Note;
import ru.ermolaeva.unotes.entities.User;
import ru.ermolaeva.unotes.forms.NoteForm;
import ru.ermolaeva.unotes.repositories.FoldersRepository;
import ru.ermolaeva.unotes.repositories.NotesRepository;
import ru.ermolaeva.unotes.services.NotesService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class NotesServicesImpl implements NotesService {

    private final NotesRepository notesRepository;
    private final FoldersRepository foldersRepository;

    @Override
    public void create(NoteForm form, Long folderId, User user) {
        Note note = Note.builder()
                .createDate(LocalDateTime.now())
                .name(form.getName())
                .note(form.getNote())
                .folder(foldersRepository.getById(folderId))
                .user(user)
                .build();

        notesRepository.save(note);
    }

    @Override
    public List<Note> getAllNotesByUser(User user) {
        return notesRepository.findAllByUserOrderByCreateDate(user);
    }

    @Override
    public Note getNoteById(Long id) {
        return notesRepository.getById(id);
    }

    @Override
    public void edit(NoteForm form, Long id, Long folderId) {
        Note currentNote = notesRepository.getById(id);

        currentNote.setName(form.getName());
        currentNote.setNote(form.getNote());
        currentNote.setFolder(foldersRepository.getById(folderId));

        notesRepository.save(currentNote);
    }

    @Override
    public List<Note> getAllNotesByFolderId(Long folderId) {
        return notesRepository.findAllByFolderIdOrderByCreateDate(folderId);
    }

    @Override
    public void delete(Long id) {
        notesRepository.deleteById(id);
    }

    @Override
    public List<Note> getNotesBasedOnWord(String word) {

        return notesRepository.findAll().stream()
                .filter(n -> (n.getNote().toLowerCase().contains(word) || n.getName().toLowerCase().contains(word)))
                .collect(Collectors.toList());
    }
}

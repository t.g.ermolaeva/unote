package ru.ermolaeva.unotes.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.ermolaeva.unotes.entities.User;
import ru.ermolaeva.unotes.forms.UserRegistrationForm;
import ru.ermolaeva.unotes.repositories.UsersRepository;
import ru.ermolaeva.unotes.services.UserRegistrationService;

import java.util.Locale;

@RequiredArgsConstructor
@Service
public class UserRegistrationServiceImpl implements UserRegistrationService {

    private final UsersRepository usersRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void registerUser(UserRegistrationForm form) {
        User user = User.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail().toLowerCase(Locale.ROOT))
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .role(User.Role.USER)
                .build();

        usersRepository.save(user);
    }
}

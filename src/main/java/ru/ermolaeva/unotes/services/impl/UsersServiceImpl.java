package ru.ermolaeva.unotes.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.ermolaeva.unotes.entities.User;
import ru.ermolaeva.unotes.forms.UserForm;
import ru.ermolaeva.unotes.repositories.UsersRepository;
import ru.ermolaeva.unotes.services.UsersService;

import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (User)authentication.getPrincipal();
    }

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public void deleteUser(Long id) {
        usersRepository.deleteById(id);
    }

    @Override
    public User getUserById(Long id) {
        return usersRepository.getById(id);
    }

    @Override
    public void updateUser(Long id, UserForm form) {
        User user = usersRepository.getById(id);

        user.setFirstName(form.getFirstName());
        user.setLastName(form.getLastName());
        user.setEmail(form.getEmail());

        usersRepository.save(user);
    }
}
